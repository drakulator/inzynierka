﻿public class PanelHotel : PlacePanel
{
    public override void OnLoad()
    {
        nameText.text = place.establishmentName;
        firstText.text = place.PriceText;
        secondText.text = place.StarsText;
        thirdText.text = place.TypeText;
        image.sprite = place.sprite;
        if (TravelPlan.Instance.HasPlace(place))
        {
            travelPlanButtonImage.sprite = removeButton;
        }
        else
        {
            travelPlanButtonImage.sprite = addButton;
        }
    }
}
