﻿using System;

public class PanelPlaceOfInterest : PlacePanel
{
    public override void OnLoad()
    {
        nameText.text = place.establishmentName;
        firstText.text = place.PriceText;
        secondText.text = place.workingHours[(int)DateTime.Now.DayOfWeek];
        thirdText.text = place.TypeText;
        image.sprite = place.sprite;
        if (TravelPlan.Instance.HasPlace(place))
        {
            travelPlanButtonImage.sprite = removeButton;
        }
        else
        {
            travelPlanButtonImage.sprite = addButton;
        }
    }
}
