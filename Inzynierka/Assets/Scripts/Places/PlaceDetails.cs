﻿using NaughtyAttributes;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlaceDetails : MonoBehaviour
{
    public PlaceType DetailsPanelType;

    [SerializeField]
    private GameObject detailsHolder;

    [SerializeField]
    private Sprite starFull;
    [SerializeField]
    private Sprite starEmpty;

    [SerializeField]
    private TextMeshProUGUI nameText;
    [SerializeField]
    private TextMeshProUGUI typeText;
    [SerializeField]
    private TextMeshProUGUI locationText;
    [SerializeField]
    private TextMeshProUGUI descriptionText;
    [SerializeField]
    private TextMeshProUGUI phoneNumberText;
    [SerializeField]
    private TextMeshProUGUI websiteText;
    [SerializeField]
    private TextMeshProUGUI pricesText;
    [SerializeField]
    private TextMeshProUGUI ratingText;
    private bool IsHotel() => DetailsPanelType == PlaceType.Hotel;
    [SerializeField]
    [ShowIf("IsHotel")]
    private GameObject starsHolder;
    [SerializeField]
    [HideIf("IsHotel")]
    private GameObject hoursHolder;

    [SerializeField]
    private Image travelPlanButtonImage;
    [SerializeField]
    private Sprite addButton;
    [SerializeField]
    private Sprite removeButton;

    [SerializeField]
    private GameObject descrpitionHolder;

    [SerializeField]
    private UserReviews reviews;

    [SerializeField]
    private Image image;

    private Place selectedPlace;

    private void OnEnable()
    {
        EventManager.AddListener<Place>("ShowDetails", OnShowDetails);
        EventManager.AddListener("HideDetails", OnHideDetails);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<Place>("ShowDetails", OnShowDetails);
        EventManager.RemoveListener("HideDetails", OnHideDetails);
    }

    private void OnShowDetails(object sender, Place place)
    {
        if (DetailsPanelType != place.PlaceType)
            return;

        detailsHolder.SetActive(true);
        nameText.gameObject.SetActive(true);

        selectedPlace = place;

        pricesText.text = place.PriceText;
        ratingText.text = place.Rating != 0 ? place.Rating.ToString() : "N/A";
        nameText.text = place.establishmentName;
        typeText.text = place.TypeText;
        locationText.text = place.AddressString;
        phoneNumberText.text = place.phoneNumber;
        websiteText.text = place.websiteLink;

        descriptionText.text = place.description;
        image.sprite = place.sprite;

        if (place.HasStars())
        {
            Image[] stars = starsHolder.GetComponentsInChildren<Image>();
            for (int i = 0; i < stars.Length; i++)
            {
                if (place.stars <= i)
                {
                    stars[i].sprite = starEmpty;
                }
                else
                {
                    stars[i].sprite = starFull;
                }
            }
        }

        if (!IsHotel())
        {
            TextMeshProUGUI[] hoursTextHolders = hoursHolder.GetComponentsInChildren<TextMeshProUGUI>().Where(x => x.name == "Hours").ToArray();
            for (int i = 0; i < place.workingHours.Length; i++)
            {
                hoursTextHolders[i].text = place.workingHours[i];
            }
        }
        if (TravelPlan.Instance.HasPlace(selectedPlace))
        {
            travelPlanButtonImage.sprite = removeButton;
        }
        else
        {
            travelPlanButtonImage.sprite = addButton;
        }
    }

    public void B_ShowOnMap()
    {
        BackButtonController.RegisterBackButton(BackButtonFunction.HideMap);
        EventManager.Invoke("LoadMap", this, selectedPlace.latitude, selectedPlace.longitude);
    }

    public void B_Hiperlink()
    {
        Application.OpenURL(websiteText.text);
    }

    private void OnHideDetails(object sender)
    {
        nameText.gameObject.SetActive(false);
        descrpitionHolder.SetActive(false);
        detailsHolder.SetActive(false);
    }

    public void B_TravelPlan()
    {
        if (TravelPlan.Instance.HasPlace(selectedPlace))
        {
            travelPlanButtonImage.sprite = addButton;
            EventManager.Invoke("RemoveFromTravelPlan", this, selectedPlace);
        }
        else
        {
            travelPlanButtonImage.sprite = removeButton;
            EventManager.Invoke("AddToTravelPlan", this, selectedPlace);
        }
    }

    public void B_Description()
    {
        descrpitionHolder.SetActive(!descrpitionHolder.activeSelf);
        if (descrpitionHolder.activeSelf)
            reviews.LoadReviews(selectedPlace);
    }
}
