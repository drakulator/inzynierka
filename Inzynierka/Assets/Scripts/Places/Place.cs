﻿using NaughtyAttributes;
using System;
using System.Linq;
using UnityEngine;

public enum PriceRange
{
    Free,
    Low,
    Average,
    High,
    VeryHigh
}

public enum PlaceType
{
    Restaurant = 1,
    Hotel = 2,
    PlaceOfInterest = 3
}

public abstract class Place : ScriptableObject
{
    [ReadOnly]
    public int id;

    public string establishmentName;
    public PlaceType PlaceType { get; protected set; }
    public bool HasStars() => PlaceType == PlaceType.Hotel;
    [ShowIf("HasStars")]
    [Range(1, 5)]
    [SerializeField]
    public int stars = 1;

    public string StarsText
    {
        get
        {
            string starsText = "";
            for (int i = 0; i < stars; i++)
            {
                starsText += "*";
            }
            return starsText;
        }
    }

    public bool hasPlaceTypeSpecification;
    public abstract string TypeText { get; }

    [TextArea]
    public string description;
    public Address address;

    public PriceRange priceRange;

    public string PriceText
    {
        get
        {
            switch (priceRange)
            {
                case PriceRange.Free:
                    return "free";
                case PriceRange.Low:
                    return "$";
                case PriceRange.Average:
                    return "$$";
                case PriceRange.High:
                    return "$$$";
                case PriceRange.VeryHigh:
                    return "$$$$";
            }
            return null;
        }
    }

    public double latitude;
    public double longitude;

    public string websiteLink;
    public string phoneNumber;

    public string AddressString => $"{address.number}{(address.hasPremiseNumber ? $"/{address.premiseNumber}" : "")} {address.street}, {address.city}, {address.country}";

    public Sprite sprite;

    public string[] workingHours = new string[]
    {
        "09:00 - 21:00",
        "09:00 - 21:00",
        "09:00 - 21:00",
        "09:00 - 21:00",
        "09:00 - 21:00",
        "09:00 - 20:00",
        "closed"
    };

    public bool SavedToTravelPlan
    {
        get
        {
            return TravelPlan.Instance != null ? TravelPlan.Instance.HasPlace(this) : false;
        }
    }

    public float Rating
    {
        get
        {
            if (UserRatings.comments.Where(x => x.placeName == establishmentName).Count() == 0)
                return 0f;
            return (float)Math.Round(UserRatings.comments.Where(x => x.placeName == establishmentName).Average(x => x.rating), 2);
        }
    }

    public CommentValues[] Comments
    {
        get
        {
            if (UserRatings.comments.Where(x => x.placeName == establishmentName).Count() == 0)
                return null;
            return UserRatings.comments.Where(x => x.placeName == establishmentName).ToArray();
        }
    }

    [Serializable]
    public class Address
    {
        public string country;
        public string city;
        public string street;

        public int number;
        public bool hasPremiseNumber;
        public int premiseNumber;

        public Address(string country, string city, string street, int number, bool hasPremiseNumber, int premiseNumber)
        {
            this.country = country;
            this.city = city;
            this.street = street;
            this.number = number;
            this.hasPremiseNumber = hasPremiseNumber;
            this.premiseNumber = premiseNumber;
        }
    }
}
