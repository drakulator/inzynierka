﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlacePanel : MonoBehaviour
{
    public Place place;

    [SerializeField]
    protected TextMeshProUGUI nameText;
    [SerializeField]
    protected TextMeshProUGUI firstText;
    [SerializeField]
    protected TextMeshProUGUI secondText;
    [SerializeField]
    protected TextMeshProUGUI thirdText;
    [SerializeField]
    protected Image image;

    [SerializeField]
    protected Image travelPlanButtonImage;
    [SerializeField]
    protected Sprite addButton;
    [SerializeField]
    protected Sprite removeButton;

    private void OnEnable()
    {
        EventManager.AddListener<Place>("AddToTravelPlan", RefreshTravelPlanButton);
        EventManager.AddListener<Place>("RemoveFromTravelPlan", RefreshTravelPlanButton);
        EventManager.AddListener<Place>("ConfirmRemoval", RemoveFromTravelPlan);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<Place>("AddToTravelPlan", RefreshTravelPlanButton);
        EventManager.RemoveListener<Place>("RemoveFromTravelPlan", RefreshTravelPlanButton);
        EventManager.RemoveListener<Place>("ConfirmRemoval", RemoveFromTravelPlan);
    }

    public virtual void OnLoad()
    {
        nameText.text = place.establishmentName;
        image.sprite = place.sprite;
        if (TravelPlan.Instance.HasPlace(place))
        {
            travelPlanButtonImage.sprite = removeButton;
        }
        else
        {
            travelPlanButtonImage.sprite = addButton;
        }
    }

    private void RefreshTravelPlanButton(object sender, Place place)
    {
        if (TravelPlan.Instance.HasPlace(this.place))
        {
            travelPlanButtonImage.sprite = removeButton;
        }
        else
        {
            travelPlanButtonImage.sprite = addButton;
        }
    }

    public void B_TravelPlan()
    {
        if (TravelPlan.Instance.HasPlace(place))
        {
            if (WarningPanel.ActiveInstance)
            {
                EventManager.Invoke("RequestRemoval", this, place);
            }
            else
            {
                travelPlanButtonImage.sprite = addButton;
                EventManager.Invoke("RemoveFromTravelPlan", this, place);
                EventManager.Invoke("ReloadTravelPlan", this);
            }
        }
        else
        {
            AddToTravelPlan();
        }
    }

    private void AddToTravelPlan()
    {
        travelPlanButtonImage.sprite = removeButton;
        EventManager.Invoke("AddToTravelPlan", this, place);
    }

    private void RemoveFromTravelPlan(object sender, Place place)
    {
        if(this.place == place)
        {
            travelPlanButtonImage.sprite = addButton;
            EventManager.Invoke("RemoveFromTravelPlan", this, place);
            EventManager.Invoke("ReloadTravelPlan", this);
        }
    }

    public void B_Details()
    {
        if(TravelPlanPanelController.IsOpen)
            BackButtonController.RegisterBackButton(BackButtonFunction.TravelPlan);
        else
            BackButtonController.RegisterBackButton(BackButtonFunction.Places);
        EventManager.Invoke("ShowDetails", this, place);
        EventManager.Invoke("HideTravelPlan", this);
        EventManager.Invoke("HidePlaces", this);
    }
}
