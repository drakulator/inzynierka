﻿using UnityEngine;

public class PlacesPanelController : MonoBehaviour
{
    [SerializeField]
    private GameObject placesHolder;

    private void OnEnable()
    {
        EventManager.AddListener("SearchButtonClicked", OnSearchButtonClicked);
        EventManager.AddListener("HidePlaces", OnHidePlaces);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener("SearchButtonClicked", OnSearchButtonClicked);
        EventManager.RemoveListener("HidePlaces", OnHidePlaces);
    }

    private void OnSearchButtonClicked(object sender)
    {
        placesHolder.SetActive(true);
    }

    private void OnHidePlaces(object sender)
    {
        placesHolder.SetActive(false);
    }
}