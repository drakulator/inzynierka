﻿using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class PlacesLoader : MonoBehaviour
{
    [SerializeField]
    private GameObject placesHolder;
    [SerializeField]
    private GameObject placePrefabHotel;
    [SerializeField]
    private GameObject placePrefabRestaurant;
    [SerializeField]
    private GameObject placePrefabPlaceOfInterest;
    [SerializeField]
    private GameObject noResultsPanel;

    private Place[] places;

    private void Awake()
    {
        places = Resources.LoadAll<Place>("");
    }

    private void OnEnable()
    {
        EventManager.AddListener("SearchButtonClicked", LoadPlaces);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener("SearchButtonClicked", LoadPlaces);
    }

    private void LoadPlaces(object sender)
    {
        LoadPlaces();
    }

    private void LoadPlaces()
    {
        foreach (Transform child in placesHolder.transform)
        {
            if(child.tag != "Persistent")
                Destroy(child.gameObject);
        }
        Place[] placesToLoad = places.Clone() as Place[];

        if (!string.IsNullOrWhiteSpace(FiltersList.nameFilter))
        {
            Regex regex = new Regex(FiltersList.nameFilter, RegexOptions.IgnoreCase);
            placesToLoad = placesToLoad.Where(x => regex.IsMatch(x.establishmentName)).ToArray();
        }
        if (!string.IsNullOrWhiteSpace(FiltersList.cityFilter))
        {
            Regex regex = new Regex(FiltersList.cityFilter, RegexOptions.IgnoreCase);
            placesToLoad = placesToLoad.Where(x => regex.IsMatch(x.address.city)).ToArray();
        }
        if (!string.IsNullOrWhiteSpace(FiltersList.countryFilter))
        {
            placesToLoad = placesToLoad.Where(x => string.Equals(x.address.country, FiltersList.countryFilter, System.StringComparison.OrdinalIgnoreCase)).ToArray();
        }
        if(FiltersList.placeTypeFilter != null)
        {
            placesToLoad = placesToLoad.Where(x => x.PlaceType == FiltersList.placeTypeFilter).ToArray();
        }
        if (FiltersList.priceRangeFilter != null)
        {
            placesToLoad = placesToLoad.Where(x => x.priceRange == FiltersList.priceRangeFilter).ToArray();
        }
        if (FiltersList.starsFilter != null && FiltersList.placeTypeFilter == PlaceType.Hotel)
        {
            placesToLoad = placesToLoad.Where(x => x.HasStars() && x.stars == FiltersList.starsFilter).ToArray();
        }
        if (FiltersList.ratingFilter != null && FiltersList.placeTypeFilter != PlaceType.PlaceOfInterest)
        {
            placesToLoad = placesToLoad.Where(x => x.Rating >= FiltersList.ratingFilter).ToArray();
        }
        if (!string.IsNullOrWhiteSpace(FiltersList.advancedTypeFilter))
        {
            placesToLoad = placesToLoad.Where(x => x.hasPlaceTypeSpecification && x.TypeText == FiltersList.advancedTypeFilter).ToArray();
        }

        if (placesToLoad.Length == 0)
            noResultsPanel.SetActive(true);
        else
        {
            GameObject placePrefab = null;
            switch (FiltersList.placeTypeFilter)
            {
                case PlaceType.Hotel:
                    placePrefab = placePrefabHotel;
                    break;
                case PlaceType.Restaurant:
                    placePrefab = placePrefabRestaurant;
                    break;
                case PlaceType.PlaceOfInterest:
                    placePrefab = placePrefabPlaceOfInterest;
                    break;
                default:
                    Debug.LogError("No place type specified!");
                    return;
            }
            noResultsPanel.SetActive(false);
            placesToLoad = placesToLoad.OrderByDescending(x => x.Rating).ToArray();
            foreach (Place place in placesToLoad)
            {
                GameObject placeInstance = Instantiate(placePrefab, placesHolder.transform);
                placeInstance.GetComponent<PlacePanel>().place = place;
                placeInstance.GetComponent<PlacePanel>().OnLoad();
            }
        }
    }
}
