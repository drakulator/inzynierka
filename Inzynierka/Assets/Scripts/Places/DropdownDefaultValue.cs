﻿using TMPro;
using UnityEngine;

public class DropdownDefaultValue : MonoBehaviour
{
    public string value;

    private TMP_Dropdown dropdown;
    private TextMeshProUGUI text;

    private void Awake()
    {
        dropdown = GetComponent<TMP_Dropdown>();
        text = GetComponentInChildren<TextMeshProUGUI>(true);
    }

    private void OnEnable()
    {
        OnDropdownChanged(dropdown.value);
        dropdown.onValueChanged.AddListener(OnDropdownChanged);
    }

    private void OnDisable()
    {
        dropdown.onValueChanged.RemoveListener(OnDropdownChanged);
    }

    public void OnDropdownChanged(int value)
    {
        if (value == 0)
        {
            text.color = new Color(1f, 1f, 1f, 0.5f);
            text.text = this.value;
        }
        else
            text.color = Color.white;
    }
}
