﻿using NaughtyAttributes;
using System.Text.RegularExpressions;
using UnityEngine;

public enum HotelTypeSpecification
{
    Motel,
    Hostel
}

[CreateAssetMenu(menuName = "Places/Hotel")]
public class Hotel : Place
{
    [ShowIf("hasPlaceTypeSpecification")]
    public HotelTypeSpecification placeTypeSpecification;

    public Hotel()
    {
        PlaceType = PlaceType.Hotel;
    }

    public override string TypeText
    {
        get
        {
            string typeText = hasPlaceTypeSpecification ? placeTypeSpecification.ToString() : PlaceType.ToString();
            return Regex.Replace(typeText.ToString(), "([a-z])([A-Z])", "$1 $2");
        }
    }
}
