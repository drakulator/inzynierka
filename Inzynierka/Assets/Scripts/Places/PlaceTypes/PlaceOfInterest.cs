﻿using NaughtyAttributes;
using System.Text.RegularExpressions;
using UnityEngine;

public enum PlaceOfInterestTypeSpecification
{
    Museum,
    Aquapark
}

[CreateAssetMenu(menuName = "Places/Place of Interest")]
public class PlaceOfInterest : Place
{
    [ShowIf("hasPlaceTypeSpecification")]
    public PlaceOfInterestTypeSpecification placeTypeSpecification;

    public PlaceOfInterest()
    {
        PlaceType = PlaceType.PlaceOfInterest;
    }

    public override string TypeText
    {
        get
        {
            string typeText = hasPlaceTypeSpecification ? placeTypeSpecification.ToString() : PlaceType.ToString();
            return Regex.Replace(typeText.ToString(), "([a-z])([A-Z])", "$1 $2");
        }
    }
}
