﻿using NaughtyAttributes;
using System.Text.RegularExpressions;
using UnityEngine;

public enum RestaurantTypeSpecification
{
    Ethnic,
    FastFood,
    Cafe,
    Bar,
    Pub
}

[CreateAssetMenu(menuName = "Places/Restaurant")]
public class Restaurant : Place
{
    [ShowIf("hasPlaceTypeSpecification")]
    public RestaurantTypeSpecification placeTypeSpecification;

    public Restaurant()
    {
        PlaceType = PlaceType.Restaurant;
    }

    public override string TypeText
    {
        get
        {
            string typeText = hasPlaceTypeSpecification ? placeTypeSpecification.ToString() : PlaceType.ToString();
            return Regex.Replace(typeText.ToString(), "([a-z])([A-Z])", "$1 $2");
        }
    }
}
