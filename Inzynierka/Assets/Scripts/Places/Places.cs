﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text.RegularExpressions;

public class Places
{
    public static string[] Names
    {
        get
        {
            HashSet<string> names = new HashSet<string>();
            foreach (Place place in Resources.LoadAll<Place>(""))
            {
                names.Add(place.establishmentName);
            }
            return names.ToArray();
        }
    }

    public static string[] Cities
    {
        get
        {
            HashSet<string> cities = new HashSet<string>();
            foreach (Place place in Resources.LoadAll<Place>(""))
            {
                cities.Add(place.address.city);
            }
            return cities.ToArray();
        }
    }

    public static string[] Countries
    {
        get
        {
            HashSet<string> countries = new HashSet<string>();
            foreach (Place place in Resources.LoadAll<Place>(""))
            {
                countries.Add(place.address.country);
            }
            return countries.ToArray();
        }
    }

    public static string[] NamesFiltered
    {
        get
        {
            Place[] placesToLoad = Resources.LoadAll<Place>("");
            HashSet<string> names = new HashSet<string>();

            if (!string.IsNullOrWhiteSpace(FiltersList.countryFilter))
            {
                placesToLoad = placesToLoad.Where(x => string.Equals(x.address.country, FiltersList.countryFilter, System.StringComparison.OrdinalIgnoreCase)).ToArray();
            }
            if (FiltersList.placeTypeFilter != null)
            {
                placesToLoad = placesToLoad.Where(x => x.PlaceType == FiltersList.placeTypeFilter).ToArray();
            }

            foreach (Place place in placesToLoad)
            {
                names.Add(place.establishmentName);
            }
            return names.ToArray();
        }
    }

    public static string[] CitiesFiltered
    {
        get
        {
            Place[] placesToLoad = Resources.LoadAll<Place>("");
            HashSet<string> cities = new HashSet<string>();

            if (!string.IsNullOrWhiteSpace(FiltersList.countryFilter))
            {
                placesToLoad = placesToLoad.Where(x => string.Equals(x.address.country, FiltersList.countryFilter, System.StringComparison.OrdinalIgnoreCase)).ToArray();
            }

            foreach (Place place in placesToLoad)
            {
                cities.Add(place.address.city);
            }
            return cities.ToArray();
        }
    }
}
