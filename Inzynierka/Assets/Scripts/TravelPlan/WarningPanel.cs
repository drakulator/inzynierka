﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WarningPanel : MonoBehaviour
{
    public static bool ActiveInstance => FindObjectOfType<WarningPanel>() != null;

    [SerializeField]
    private GameObject warningHolder;
    [SerializeField]
    private TextMeshProUGUI text;

    private Place selectedPlace;

    private void OnEnable()
    {
        EventManager.AddListener<Place>("RequestRemoval", OnRequestRemoval);
    }

    private void OnDisable()
    {
        warningHolder.SetActive(false);
        EventManager.RemoveListener<Place>("RequestRemoval", OnRequestRemoval);
    }

    private void OnRequestRemoval(object sender, Place place)
    {
        selectedPlace = place;
        text.text = $"Are you sure you want to remove {place.establishmentName} from your travel plan?";
        warningHolder.SetActive(true);
    }

    public void B_Confirm()
    {
        EventManager.Invoke("ConfirmRemoval", this, selectedPlace);
        warningHolder.SetActive(false);
    }

    public void B_Decline()
    {
        warningHolder.SetActive(false);
    }
}
