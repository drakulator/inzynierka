﻿using System.Linq;
using UnityEngine;

public class TravelPlanLoader : MonoBehaviour
{
    [SerializeField]
    private GameObject placesHolder;
    [SerializeField]
    private GameObject placePrefabHotel;
    [SerializeField]
    private GameObject placePrefabRestaurant;
    [SerializeField]
    private GameObject placePrefabPlaceOfInterest;
    [SerializeField]
    private GameObject noResultsPanel;

    private Place[] places;

    private void Awake()
    {
        places = Resources.LoadAll<Place>("");
    }

    private void OnEnable()
    {
        EventManager.AddListener("TravelPlanButtonClicked", ReloadPlaces);
        EventManager.AddListener("ReloadTravelPlan", ReloadPlaces);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener("TravelPlanButtonClicked", ReloadPlaces);
        EventManager.RemoveListener("ReloadTravelPlan", ReloadPlaces);
    }

    private void ReloadPlaces(object sender)
    {
        LoadPlaces();
    }

    private void LoadPlaces()
    {
        foreach (Transform child in placesHolder.transform)
        {
            if (child.tag != "Persistent")
                Destroy(child.gameObject);
        }
        Place[] placesToLoad = places.Clone() as Place[];

        placesToLoad = placesToLoad.Where(x => x.SavedToTravelPlan).ToArray();
        if (!string.IsNullOrWhiteSpace(FiltersList.countryFilter))
        {
            placesToLoad = placesToLoad.Where(x => string.Equals(x.address.country, FiltersList.countryFilter, System.StringComparison.OrdinalIgnoreCase)).ToArray();
        }

        if (placesToLoad.Length == 0)
            noResultsPanel.SetActive(true);
        else
        {
            noResultsPanel.SetActive(false);
            foreach (Place place in placesToLoad)
            {
                GameObject placeInstance = null;
                switch (place.PlaceType)
                {
                    case PlaceType.Hotel:
                        placeInstance = Instantiate(placePrefabHotel, placesHolder.transform);
                        break;
                    case PlaceType.Restaurant:
                        placeInstance = Instantiate(placePrefabRestaurant, placesHolder.transform);
                        break;
                    case PlaceType.PlaceOfInterest:
                        placeInstance = Instantiate(placePrefabPlaceOfInterest, placesHolder.transform);
                        break;
                }
                placeInstance.GetComponent<PlacePanel>().place = place;
                placeInstance.GetComponent<PlacePanel>().OnLoad();
            }
        }
    }
}
