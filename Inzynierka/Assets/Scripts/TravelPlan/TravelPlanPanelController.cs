﻿using UnityEngine;

public class TravelPlanPanelController : MonoBehaviour
{
    public static bool IsOpen { get; private set; }

    [SerializeField]
    private GameObject placesHolder;

    private void OnEnable()
    {
        EventManager.AddListener("TravelPlanButtonClicked", OnTravelPlanButtonClicked);
        EventManager.AddListener("HideTravelPlan", OnHideTravelPlan);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener("TravelPlanButtonClicked", OnTravelPlanButtonClicked);
        EventManager.RemoveListener("HideTravelPlan", OnHideTravelPlan);
    }

    private void OnTravelPlanButtonClicked(object sender)
    {
        IsOpen = true;
        placesHolder.SetActive(true);
    }

    private void OnHideTravelPlan(object sender)
    {
        IsOpen = false;
        placesHolder.SetActive(false);
    }
}