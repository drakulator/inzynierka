﻿using NaughtyAttributes;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
#endif

[Serializable]
public class TravelPlan : MonoBehaviour
{
    public static TravelPlan Instance;
    private HashSet<int> travelPlan;

    private static string pathToSave { get { return Application.persistentDataPath + "/travelPlan.dat"; } }

    private void OnEnable()
    {
        EventManager.AddListener<Place>("AddToTravelPlan", OnAddToTravelPlan);
        EventManager.AddListener<Place>("RemoveFromTravelPlan", OnRemoveFromTravelPlan);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<Place>("AddToTravelPlan", OnAddToTravelPlan);
        EventManager.RemoveListener<Place>("RemoveFromTravelPlan", OnRemoveFromTravelPlan);
    }

    private void OnAddToTravelPlan(object sender, Place place)
    {
        travelPlan.Add(place.id);
        SaveTravelPlan(pathToSave);
    }

    private void OnRemoveFromTravelPlan(object sender, Place place)
    {
        travelPlan.Remove(place.id);
        SaveTravelPlan(pathToSave);
    }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            LoadTravelPlan(pathToSave);
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public bool HasPlace(Place place)
    {
        return travelPlan.Contains(place.id);
    }

    private void OnApplicationQuit()
    {
        SaveTravelPlan(pathToSave);
    }

    private void SaveTravelPlan(string filePath)
    {
        using (FileStream stream = File.Open(filePath, FileMode.Create))
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            binaryFormatter.Serialize(stream, travelPlan);
        }
    }

    private void LoadTravelPlan(string filePath)
    {
        using (FileStream stream = File.Open(filePath, FileMode.OpenOrCreate))
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            if (stream.Length != 0)
                travelPlan = (HashSet<int>)binaryFormatter.Deserialize(stream);
            else
                travelPlan = new HashSet<int>();
        }
    }

#if UNITY_EDITOR
    [Button]
    public static void IndexPlaces()
    {
        File.Delete(pathToSave);
        Place[] places = Resources.LoadAll<Place>("");
        foreach (Place place in places)
        {
            int i = 1;
            if(place.id == 0)
            {
                while(places.Count(x => x.id == i) > 0)
                {
                    i++;
                }
                place.id = i;
            }
            EditorUtility.SetDirty(place);
        }
        AssetDatabase.SaveAssets();
    }

    private class MyCustomBuildProcessor : IPreprocessBuildWithReport
    {
        public int callbackOrder { get { return 0; } }
        public void OnPreprocessBuild(BuildReport report)
        {
            IndexPlaces();
        }
    }
#endif
}
