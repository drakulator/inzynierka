﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class DragMap : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    //[SerializeField]
    //float zoomSpeed = float.MaxValue;
    [SerializeField]
    private float minTimeBetweenMapUpdates = 0.5f;

    private float lastPositionX;
    private float lastPositionY;

    private Coroutine lastMapUpdate;  

    //unused due to rewised zoom system
    /*
    private void Update()
    {
        // If there are two touches on the device...
        if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            if (lastMapUpdate != null)
                StopCoroutine(lastMapUpdate);
            lastMapUpdate = StartCoroutine(UpdateMap((int)(-deltaMagnitudeDiff * zoomSpeed)));
        }
    }
    */

    public void OnBeginDrag(PointerEventData eventData)
    {
        lastPositionX = eventData.position.x;
        lastPositionY = eventData.position.y;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (lastMapUpdate != null)
            StopCoroutine(lastMapUpdate);
        lastMapUpdate = StartCoroutine(UpdateMap(eventData));
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (lastMapUpdate != null)
            StopCoroutine(lastMapUpdate);
        lastMapUpdate = StartCoroutine(UpdateMap(eventData));
    }

    private IEnumerator UpdateMap(PointerEventData eventData)
    {
        yield return new WaitForSeconds(minTimeBetweenMapUpdates);
        double positionDifferenceX = eventData.position.x - lastPositionX;
        double positionDifferenceY = eventData.position.y - lastPositionY;
        EventManager.Invoke("SwipeMap", this, positionDifferenceX, positionDifferenceY);
        lastPositionX = eventData.position.x;
        lastPositionY = eventData.position.y;
    }

    private IEnumerator UpdateMap(int zoom)
    {
        yield return new WaitForSeconds(minTimeBetweenMapUpdates);
        EventManager.Invoke("ZoomMap", this, zoom);
    }
}
