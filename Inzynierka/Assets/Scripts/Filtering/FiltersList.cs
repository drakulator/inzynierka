﻿public static class FiltersList
{
    public static string nameFilter;
    public static string cityFilter;
    public static string countryFilter;

    public static PlaceType? placeTypeFilter = null;
    public static PriceRange? priceRangeFilter = null;
    public static string advancedTypeFilter = null;

    public static int? starsFilter = null;
    public static int? ratingFilter = null;

    public static void ResetFilters(bool resetCountry = true)
    {
        nameFilter = "";
        cityFilter = "";
        if(resetCountry)
            countryFilter = "";

        placeTypeFilter = null;
        priceRangeFilter = null;
        advancedTypeFilter = null;
        ratingFilter = null;

        starsFilter = null;
    }
}
