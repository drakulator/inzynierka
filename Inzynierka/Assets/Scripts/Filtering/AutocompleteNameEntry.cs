﻿using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class AutocompleteNameEntry : MonoBehaviour
{
    public GameObject autocompleteEntriesNameHolder;
    public GameObject autocompleteEntry;

    public void HideAutocompleteNameEntries()
    {
        autocompleteEntriesNameHolder.SetActive(false);
    }

    public void ShowAutocompleteNameEntries(string text)
    {
        autocompleteEntriesNameHolder.SetActive(true);
        foreach (Transform child in autocompleteEntriesNameHolder.transform)
        {
            Destroy(child.gameObject);
        }

        text = text.Replace("(", "\\(");
        Regex regex = new Regex(text, RegexOptions.IgnoreCase);
        string[] namesToLoad = Places.NamesFiltered.Where(x => regex.IsMatch(x)).ToArray();
        foreach (string name in namesToLoad)
        {
            GameObject instance = Instantiate(autocompleteEntry, autocompleteEntriesNameHolder.transform);
            instance.GetComponentInChildren<AutocompleteEntry>().Initiate(name, AutocompleteType.Name);
        }
    }
}
