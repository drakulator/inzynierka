﻿using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AutocompleteNameEntry))]
[RequireComponent(typeof(AutocompleteCityEntry))]
public class SearchPanel : MonoBehaviour
{
    public PlaceType SearchPanelType;

    [SerializeField]
    private TMP_InputField nameInput;
    [SerializeField]
    private TMP_InputField cityInput;

    [SerializeField]
    private TextMeshProUGUI nameText;
    [SerializeField]
    private TextMeshProUGUI cityText;
    [SerializeField]
    private TMP_Dropdown typeDropdown;
    [SerializeField]
    private TMP_Dropdown priceDropdown;
    private bool StarsActive() => SearchPanelType == PlaceType.Hotel;
    [SerializeField]
    [ShowIf("StarsActive")]
    private TMP_Dropdown starsDropdown;
    [SerializeField]
    private TMP_Dropdown ratingDropdown;

    private AutocompleteCityEntry autocompleteCity;
    private AutocompleteNameEntry autocompleteName;

    private static bool hideDropdownsOnTouch = false;

    private void Awake()
    {
        autocompleteName = GetComponent<AutocompleteNameEntry>();
        autocompleteCity = GetComponent<AutocompleteCityEntry>();
    }

    private void Update()
    {
        if(hideDropdownsOnTouch && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            hideDropdownsOnTouch = false;
            StartCoroutine(OnHideNameDropdown());
            StartCoroutine(OnHideCityDropdown());
        }
    }

    private void OnEnable()
    {
        typeDropdown.onValueChanged.AddListener(OnTypeChanged);
        priceDropdown.onValueChanged.AddListener(OnPriceChanged);
        if(StarsActive())
            starsDropdown.onValueChanged.AddListener(OnStarsChanged);
        ratingDropdown.onValueChanged.AddListener(OnRatingChanged);
        nameInput.onValueChanged.AddListener(OnNameValueChanged);
        cityInput.onValueChanged.AddListener(OnCityValueChanged);
        EventManager.AddListener<string>("NameAutocompleteSelected", OnNameAutocompleteSelected);
        EventManager.AddListener<string>("CityAutocompleteSelected", OnCityAutocompleteSelected);
        InitiateTypes();
        ClearFilters();
    }

    private void OnDisable()
    {
        typeDropdown.onValueChanged.RemoveListener(OnTypeChanged);
        priceDropdown.onValueChanged.RemoveListener(OnPriceChanged);
        if (StarsActive())
            starsDropdown.onValueChanged.RemoveListener(OnStarsChanged);
        ratingDropdown.onValueChanged.RemoveListener(OnRatingChanged);
        nameInput.onValueChanged.RemoveListener(OnNameValueChanged);
        cityInput.onValueChanged.RemoveListener(OnCityValueChanged);
        EventManager.RemoveListener<string>("NameAutocompleteSelected", OnNameAutocompleteSelected);
        EventManager.RemoveListener<string>("CityAutocompleteSelected", OnCityAutocompleteSelected);
    }

    private void OnTypeChanged(int value)
    {
        if (value == 0)
            typeDropdown.GetComponentInChildren<TextMeshProUGUI>(true).color = new Color(1f, 1f, 1f, 0.5f);
        else
            typeDropdown.GetComponentInChildren<TextMeshProUGUI>(true).color = Color.white;
    }

    private void OnPriceChanged(int value)
    {
        if (value == 0)
            priceDropdown.GetComponentInChildren<TextMeshProUGUI>(true).color = new Color(1f, 1f, 1f, 0.5f);
        else
            priceDropdown.GetComponentInChildren<TextMeshProUGUI>(true).color = Color.white;
    }

    private void OnStarsChanged(int value)
    {
        if (value == 0)
            starsDropdown.GetComponentInChildren<TextMeshProUGUI>(true).color = new Color(1f, 1f, 1f, 0.5f);
        else
            starsDropdown.GetComponentInChildren<TextMeshProUGUI>(true).color = Color.white;
    }

    private void OnRatingChanged(int value)
    {
        if (value == 0)
            ratingDropdown.GetComponentInChildren<TextMeshProUGUI>(true).color = new Color(1f, 1f, 1f, 0.5f);
        else
            ratingDropdown.GetComponentInChildren<TextMeshProUGUI>(true).color = Color.white;
    }

    public void B_Search()
    {
        FiltersList.nameFilter = nameText.text.Remove(nameText.text.Length - 1);
        FiltersList.cityFilter = cityText.text.Remove(cityText.text.Length - 1);
        switch (priceDropdown.options[priceDropdown.value].text)
        {
            case "Free":
                FiltersList.priceRangeFilter = PriceRange.Free;
                break;
            case "$":
                FiltersList.priceRangeFilter = PriceRange.Low;
                break;
            case "$$":
                FiltersList.priceRangeFilter = PriceRange.Average;
                break;
            case "$$$":
                FiltersList.priceRangeFilter = PriceRange.High;
                break;
            case "$$$$":
                FiltersList.priceRangeFilter = PriceRange.VeryHigh;
                break;
            default:
                FiltersList.priceRangeFilter = null;
                break;
        }
        if (StarsActive())
        {
            switch (starsDropdown.options[starsDropdown.value].text)
            {
                case "*":
                    FiltersList.starsFilter = 1;
                    break;
                case "**":
                    FiltersList.starsFilter = 2;
                    break;
                case "***":
                    FiltersList.starsFilter = 3;
                    break;
                case "****":
                    FiltersList.starsFilter = 4;
                    break;
                case "*****":
                    FiltersList.starsFilter = 5;
                    break;
                default:
                    FiltersList.starsFilter = null;
                    break;
            }
        }
        switch (ratingDropdown.options[ratingDropdown.value].text)
        {
            case "*":
                FiltersList.ratingFilter = 1;
                break;
            case "**":
                FiltersList.ratingFilter = 2;
                break;
            case "***":
                FiltersList.ratingFilter = 3;
                break;
            case "****":
                FiltersList.ratingFilter = 4;
                break;
            case "*****":
                FiltersList.ratingFilter = 5;
                break;
            default:
                FiltersList.ratingFilter = null;
                break;
        }
        FiltersList.advancedTypeFilter = typeDropdown.value != 0 ? typeDropdown.options[typeDropdown.value].text : null;
        BackButtonController.RegisterBackButton(BackButtonFunction.Search);
        BackButtonController.detailsAccessType = DetailsAccessType.Places;
        EventManager.Invoke("SearchButtonClicked", this);
    }

    public void OnNameValueChanged(string value)
    {
        StartCoroutine(UpdateNameValue(value));
    }

    public void OnCityValueChanged(string value)
    {
        StartCoroutine(UpdateCityValue(value));
    }

    public void HideNameDropdown()
    {
        hideDropdownsOnTouch = true;
    }

    public void HideCityDropdown()
    {
        hideDropdownsOnTouch = true;
    }

    private IEnumerator UpdateNameValue(string value)
    {
        yield return new WaitForSeconds(0.1f);
        if (!string.IsNullOrWhiteSpace(value))
        {
            string name = value;
            autocompleteName.ShowAutocompleteNameEntries(name);
        }
    }

    private IEnumerator OnHideNameDropdown()
    {
        yield return new WaitForSeconds(0.1f);
        autocompleteName.HideAutocompleteNameEntries();
    }

    private IEnumerator UpdateCityValue(string value)
    {
        yield return new WaitForSeconds(0.1f);
        if (!string.IsNullOrWhiteSpace(value))
        {
            string name = value;
            autocompleteCity.ShowAutocompleteCityEntries(name);
        }
    }

    private IEnumerator OnHideCityDropdown()
    {
        yield return new WaitForSeconds(0.1f);
        autocompleteCity.HideAutocompleteCityEntries();
    }

    private void OnNameAutocompleteSelected(object sender, string text)
    {
        nameInput.onValueChanged.RemoveListener(OnNameValueChanged);
        nameInput.text = text;
        HideNameDropdown();
        nameInput.onValueChanged.AddListener(OnNameValueChanged);
    }

    private void OnCityAutocompleteSelected(object sender, string text)
    {
        cityInput.onValueChanged.RemoveListener(OnCityValueChanged);
        cityInput.text = text;
        HideCityDropdown();
        cityInput.onValueChanged.AddListener(OnCityValueChanged);
    }

    [Button]
    public void ClearFilters()
    {
        nameInput.text = "";
        cityInput.text = "";
        typeDropdown.value = 0;
        priceDropdown.value = 0;
        if (StarsActive())
            starsDropdown.value = 0;
        ratingDropdown.value = 0;

        OnTypeChanged(0);
        OnPriceChanged(0);
        if(StarsActive())
            OnStarsChanged(0);
        OnRatingChanged(0);
    }

    private void InitiateTypes()
    {
        typeDropdown.ClearOptions();
        List<string> options = new List<string>();
        options.Add("-");

        if (SearchPanelType == PlaceType.Hotel)
        {
            foreach (HotelTypeSpecification item in Enum.GetValues(typeof(HotelTypeSpecification)))
            {
                options.Add(Regex.Replace(item.ToString(), "([a-z])([A-Z])", "$1 $2"));
            }
        }
        else if (SearchPanelType == PlaceType.Restaurant)
        {
            foreach (RestaurantTypeSpecification item in Enum.GetValues(typeof(RestaurantTypeSpecification)))
            {
                options.Add(Regex.Replace(item.ToString(), "([a-z])([A-Z])", "$1 $2"));
            }
        }
        else if (SearchPanelType == PlaceType.PlaceOfInterest)
        {
            foreach (PlaceOfInterestTypeSpecification item in Enum.GetValues(typeof(PlaceOfInterestTypeSpecification)))
            {
                options.Add(Regex.Replace(item.ToString(), "([a-z])([A-Z])", "$1 $2"));
            }
        }

        typeDropdown.AddOptions(options);
    }
}
