﻿using TMPro;
using UnityEngine;

public enum AutocompleteType
{
    Name,
    City
}

public class AutocompleteEntry : MonoBehaviour
{
    private AutocompleteType autocompleteType;

    public void Initiate(string text, AutocompleteType autocompleteType)
    {
        GetComponent<TextMeshProUGUI>().text = text;
        this.autocompleteType = autocompleteType;
    }

    public void B_Clicked()
    {
        string text = GetComponent<TextMeshProUGUI>().text;
        if (autocompleteType == AutocompleteType.Name)
            EventManager.Invoke("NameAutocompleteSelected", this, text);
        if (autocompleteType == AutocompleteType.City)
            EventManager.Invoke("CityAutocompleteSelected", this, text);
    }
}
