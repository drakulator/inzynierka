﻿using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class AutocompleteCityEntry : MonoBehaviour
{
    public GameObject autocompleteEntriesCityHolder;
    public GameObject invalidCityNameHolder;
    public GameObject autocompleteEntry;

    public void HideAutocompleteCityEntries()
    {
        autocompleteEntriesCityHolder.SetActive(false);
    }

    public bool IsCityTextValid(string text)
    {
        return Regex.IsMatch(text, @"^(\p{L}+(?:.|-| |'))*\p{L}*$", RegexOptions.IgnoreCase);
    }

    public void ShowAutocompleteCityEntries(string text)
    {
        text = text.Replace("(", "\\(");
        if (IsCityTextValid(text))
        {
            invalidCityNameHolder.SetActive(false);
            autocompleteEntriesCityHolder.SetActive(true);
            foreach (Transform child in autocompleteEntriesCityHolder.transform)
            {
                if (child.tag != "Persistent")
                    Destroy(child.gameObject);
            }

            Regex regex = new Regex(text, RegexOptions.IgnoreCase);
            string[] namesToLoad = Places.CitiesFiltered.Where(x => regex.IsMatch(x)).ToArray();
            foreach (string name in namesToLoad)
            {
                GameObject instance = Instantiate(autocompleteEntry, autocompleteEntriesCityHolder.transform);
                instance.GetComponentInChildren<AutocompleteEntry>().Initiate(name, AutocompleteType.City);
            }
        }
        else
        {
            invalidCityNameHolder.SetActive(true);
            autocompleteEntriesCityHolder.SetActive(true);
            foreach (Transform child in autocompleteEntriesCityHolder.transform)
            {
                if (child.tag != "Persistent")
                    Destroy(child.gameObject);
            }
        }
    }
}
