﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SearchToggleTypes : MonoBehaviour
{
    [SerializeField]
    private ToggleGroup toggleGroup;

    [SerializeField]
    private Toggle toggleEat;
    [SerializeField]
    private Toggle toggleSleep;
    [SerializeField]
    private Toggle toggleExplore;

    [SerializeField]
    private GameObject panelFood;
    [SerializeField]
    private GameObject panelSleep;
    [SerializeField]
    private GameObject panelExplore;

    [SerializeField]
    private GameObject panelMain;

    private void OnEnable()
    {
        EventManager.AddListener("OpenSearchPanel", OpenPanel);
        EventManager.AddListener("SearchButtonClicked", HidePanel);
        EventManager.AddListener("HideSearchPanel", HidePanel);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener("OpenSearchPanel", OpenPanel);
        EventManager.RemoveListener("SearchButtonClicked", HidePanel);
        EventManager.RemoveListener("HideSearchPanel", HidePanel);
    }

    private void HidePanel(object sender)
    {
        panelMain.SetActive(false);
    }

    private void OpenPanel(object sender)
    {
        OnToggleChanged("eat");
        panelMain.SetActive(true);
    }

    public void OnToggleChanged(string value)
    {
        switch (value)
        {
            case "eat":
                FiltersList.placeTypeFilter = PlaceType.Restaurant;
                Activate(toggleEat);
                panelFood.SetActive(true);
                Deactivate(toggleSleep);
                panelSleep.SetActive(false);
                Deactivate(toggleExplore);
                panelExplore.SetActive(false);
                break;
            case "sleep":
                FiltersList.placeTypeFilter = PlaceType.Hotel;
                Deactivate(toggleEat);
                panelFood.SetActive(false);
                Activate(toggleSleep);
                panelSleep.SetActive(true);
                Deactivate(toggleExplore);
                panelExplore.SetActive(false);
                break;
            case "explore":
                FiltersList.placeTypeFilter = PlaceType.PlaceOfInterest;
                Deactivate(toggleEat);
                panelFood.SetActive(false);
                Deactivate(toggleSleep);
                panelSleep.SetActive(false);
                Activate(toggleExplore);
                panelExplore.SetActive(true);
                break;
        }
    }

    private void Activate(Toggle toggle)
    {
        toggle.GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
        toggle.GetComponentInChildren<TextMeshProUGUI>().fontStyle = toggle.GetComponentInChildren<TextMeshProUGUI>().fontStyle | FontStyles.Underline;
    }

    private void Deactivate(Toggle toggle)
    {
        toggle.GetComponentInChildren<TextMeshProUGUI>().color = Color.gray;
        toggle.GetComponentInChildren<TextMeshProUGUI>().fontStyle = toggle.GetComponentInChildren<TextMeshProUGUI>().fontStyle & ~FontStyles.Underline;
    }
}
