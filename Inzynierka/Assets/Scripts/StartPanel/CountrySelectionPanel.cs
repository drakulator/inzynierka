﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CountrySelectionPanel : MonoBehaviour
{
    [SerializeField]
    private GameObject selectionHolder;
    [SerializeField]
    private TMP_Dropdown countryDropdown;

    private void OnEnable()
    {
        countryDropdown.onValueChanged.AddListener(OnCountryValueChanged);
        EventManager.AddListener("OpenCountrySelection", OnCountrySelectionOpened);
    }

    private void OnDisable()
    {
        countryDropdown.onValueChanged.RemoveListener(OnCountryValueChanged);
        EventManager.RemoveListener("OpenCountrySelection", OnCountrySelectionOpened);
    }

    // Use this for initialization
    void Start () {
        countryDropdown.ClearOptions();
        countryDropdown.AddOptions(new List<string>(Places.Countries));
        selectionHolder.SetActive(true);
        FiltersList.countryFilter = countryDropdown.options[countryDropdown.value].text;
    }
	
    public void B_Go()
    {
        selectionHolder.SetActive(false);
        FiltersList.countryFilter = countryDropdown.options[countryDropdown.value].text;
        FiltersList.ResetFilters(false);
        BackButtonController.RegisterBackButton(BackButtonFunction.Country);
        EventManager.Invoke("OpenSearchPanel", this);
    }

    private void OnCountrySelectionOpened(object sender)
    {
        selectionHolder.SetActive(true);
    }

    public void OnCountryValueChanged(int value)
    {
        FiltersList.countryFilter = countryDropdown.options[value].text;
    }
}
