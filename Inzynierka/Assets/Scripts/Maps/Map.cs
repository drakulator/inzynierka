﻿using NaughtyAttributes;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Map : MonoBehaviour
{
    [SerializeField]
    private double latitude;
    [SerializeField]
    private double longitude;
    [SerializeField]
    private int zoom;

    private double placeLatitude;
    private double placeLongitude;

    private int MapWidth => (int)(mapHeight * (Camera.main.aspect));
    private readonly int mapHeight = 640;

    private readonly int minZoom = 8;
    private readonly int maxZoom = 17;

    private RawImage mapImage;
    private Coroutine mapLoadingCoroutine;

    [SerializeField]
    private GameObject mapHolder;
    [SerializeField]
    private GameObject noMapHolder;

    private float swipeMagnitudeReduction = 1000f;

    private void OnEnable()
    {
        EventManager.AddListener<double, double>("LoadMap", OnLoadMap);
        EventManager.AddListener<int>("ZoomMap", OnZoomMap);
        EventManager.AddListener<double, double>("SwipeMap", OnSwipeMap);
        EventManager.AddListener("HideMap", OnHideMap);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<double, double>("LoadMap", OnLoadMap);
        EventManager.RemoveListener<int>("ZoomMap", OnZoomMap);
        EventManager.RemoveListener<double, double>("SwipeMap", OnSwipeMap);
        EventManager.RemoveListener("HideMap", OnHideMap);
    }

    private void Start()
    {
        mapImage = GetComponentInChildren<RawImage>(true);
    }

    private void OnLoadMap(object sender, double latitude, double longitude)
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            noMapHolder.SetActive(true);
            return;
        }
        noMapHolder.SetActive(false);
        mapHolder.SetActive(true);
        this.latitude = latitude;
        this.longitude = longitude;
        placeLatitude = latitude;
        placeLongitude = longitude;
        zoom = maxZoom;
        if (mapLoadingCoroutine != null)
            StopCoroutine(mapLoadingCoroutine);
        mapLoadingCoroutine = StartCoroutine(LoadMap());
    }

    private IEnumerator LoadMap()   
    {   
        string url = $"https://maps.googleapis.com/maps/api/staticmap?" +
            $"center={latitude},{longitude}&zoom={zoom}&scale=2&size={MapWidth}x{mapHeight}" +
            $"&markers=icon|{placeLatitude},{placeLongitude}&key=AIzaSyAQmBmcx8eaVacZzlFvousKEf9mZgg8YN8";
        WWW www = new WWW(url);
        yield return www;
        mapImage.texture = www.texture;
    }

    private void OnZoomMap(object sender, int zoom)
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            noMapHolder.SetActive(true);
            return;
        }
        noMapHolder.SetActive(false);
        this.zoom += zoom;
        this.zoom = Mathf.Clamp(this.zoom, minZoom, maxZoom);
        if (mapLoadingCoroutine != null)
            StopCoroutine(mapLoadingCoroutine);
        mapLoadingCoroutine = StartCoroutine(LoadMap());
    }

    private void OnSwipeMap(object sender, double magnitudeX, double magnitudeY)
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            noMapHolder.SetActive(true);
            return;
        }
        noMapHolder.SetActive(false);
        longitude += -magnitudeX / (double)zoom / swipeMagnitudeReduction;
        latitude += -magnitudeY / (double)zoom / swipeMagnitudeReduction;

        FixCoordinates(ref latitude, ref longitude);

        if (mapLoadingCoroutine != null)
            StopCoroutine(mapLoadingCoroutine);
        mapLoadingCoroutine = StartCoroutine(LoadMap());
    }

    private void FixCoordinates(ref double latitude, ref double longitude)
    {
        while (latitude < -90d)
        {
            latitude += 180d;
        }
        while (latitude > 90d)
        {
            latitude -= 180d;
        }
        while (longitude < -180d)
        {
            longitude += 360d;
        }
        while (longitude > 180d)
        {
            longitude -= 360d;
        }
    }

    private void OnHideMap(object sender)
    {
        noMapHolder.SetActive(false);
        mapHolder.SetActive(false);
    }

    [Button]
    public void B_ZoomIn()
    {
        OnZoomMap(this, 1);
    }

    [Button]
    public void B_ZoomOut()
    {
        OnZoomMap(this, -1);
    }

    [Button]
    public void B_CenterOnPlace()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            noMapHolder.SetActive(true);
            return;
        }
        noMapHolder.SetActive(false);
        latitude = placeLatitude;
        longitude = placeLongitude;
        zoom = maxZoom;
        if (mapLoadingCoroutine != null)
            StopCoroutine(mapLoadingCoroutine);
        mapLoadingCoroutine = StartCoroutine(LoadMap());
    }
}
