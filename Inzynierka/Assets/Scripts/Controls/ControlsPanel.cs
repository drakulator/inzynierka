﻿using UnityEngine;

public class ControlsPanel : MonoBehaviour
{
    public void B_TravelPlan()
    {
        BackButtonController.RegisterBackButton(BackButtonFunction.Search);
        BackButtonController.detailsAccessType = DetailsAccessType.TravelPlan;
        EventManager.Invoke("TravelPlanButtonClicked", this);
        EventManager.Invoke("HideMap", this);
        EventManager.Invoke("HideDetails", this);
        EventManager.Invoke("HidePlaces", this);
        EventManager.Invoke("HideSearchPanel", this);
    }

    public void B_Search()
    {
        BackButtonController.RegisterBackButton(BackButtonFunction.Country);
        EventManager.Invoke("OpenSearchPanel", this);
        EventManager.Invoke("HideMap", this);
        EventManager.Invoke("HideDetails", this);
        EventManager.Invoke("HidePlaces", this);
        EventManager.Invoke("HideTravelPlan", this);
    }
   
    public void B_Quit()
    {
        Application.Quit();
    }

    public void B_CountrySelectionButton()
    {
        BackButtonController.RegisterBackButton(BackButtonFunction.None);
        EventManager.Invoke("OpenCountrySelection", this);
        EventManager.Invoke("HideMap", this);
        EventManager.Invoke("HideDetails", this);
        EventManager.Invoke("HidePlaces", this);
        EventManager.Invoke("HideTravelPlan", this);
    }
}
