﻿using UnityEngine;

public enum BackButtonFunction
{
    None,
    Country,
    Search,
    Places,
    TravelPlan,
    HideMap
}

public enum DetailsAccessType
{
    Places,
    TravelPlan
}

public class BackButtonController : MonoBehaviour 
{
    private static BackButtonFunction backButtonFunction = BackButtonFunction.None;
    public static DetailsAccessType detailsAccessType;

    public static void RegisterBackButton(BackButtonFunction backButtonFunction)
    {
        BackButtonController.backButtonFunction = backButtonFunction;
    }

    public void B_Back()
    {
        switch (backButtonFunction)
        {
            case BackButtonFunction.None:
                break;
            case BackButtonFunction.Country:
                backButtonFunction = BackButtonFunction.None;
                EventManager.Invoke("OpenCountrySelection", this);
                break;
            case BackButtonFunction.Search:
                backButtonFunction = BackButtonFunction.Country;
                EventManager.Invoke("HidePlaces", this);
                EventManager.Invoke("HideTravelPlan", this);
                EventManager.Invoke("OpenSearchPanel", this);
                break;
            case BackButtonFunction.Places:
                backButtonFunction = BackButtonFunction.Search;
                EventManager.Invoke("HideDetails", this);
                EventManager.Invoke("SearchButtonClicked", this);
                break;
            case BackButtonFunction.TravelPlan:
                backButtonFunction = BackButtonFunction.Search;
                EventManager.Invoke("HideDetails", this);
                EventManager.Invoke("TravelPlanButtonClicked", this);
                break;
            case BackButtonFunction.HideMap:
                if(detailsAccessType == DetailsAccessType.Places)
                {
                    backButtonFunction = BackButtonFunction.Places;
                    EventManager.Invoke("HideMap", this);
                }
                else if (detailsAccessType == DetailsAccessType.TravelPlan)
                {
                    backButtonFunction = BackButtonFunction.TravelPlan;
                    EventManager.Invoke("HideMap", this);
                }
                break;
        }
    }
}