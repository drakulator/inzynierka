﻿using System.Collections.Generic;
using UnityEngine;

public class EventManager
{
    public delegate void Listener(object sender);
    public delegate void Listener<T>(object sender, T t);
    public delegate void Listener<T1, T2>(object sender, T1 t1, T2 t2);

    public delegate ReturnType Getter<ReturnType>(object sender);

    public static void AddListener(string eventName, Listener listener)
    {
        if (GenericEvents.events.ContainsKey(eventName))
            GenericEvents.events[eventName] += listener;
        else
            GenericEvents.events.Add(eventName, listener);
    }

    public static void AddListener<T>(string eventName, Listener<T> listener)
    {
        if (GenericEvents<T>.events.ContainsKey(eventName))
            GenericEvents<T>.events[eventName] += listener;
        else
            GenericEvents<T>.events.Add(eventName, listener);
    }

    public static void AddListener<T1, T2>(string eventName, Listener<T1, T2> listener)
    {
        if (GenericEvents<T1, T2>.events.ContainsKey(eventName))
            GenericEvents<T1, T2>.events[eventName] += listener;
        else
            GenericEvents<T1, T2>.events.Add(eventName, listener);
    }

    public static void RemoveListener(string eventName, Listener listener)
    {
        if (GenericEvents.events.ContainsKey(eventName))
        {
            GenericEvents.events[eventName] -= listener;
            if(GenericEvents.events[eventName] == null)
            {
                GenericEvents.events.Remove(eventName);
            }
        }
    }

    public static void RemoveListener<T>(string eventName, Listener<T> listener)
    {
        if (GenericEvents<T>.events.ContainsKey(eventName))
        {
            GenericEvents<T>.events[eventName] -= listener;
            if (GenericEvents<T>.events[eventName] == null)
            {
                GenericEvents<T>.events.Remove(eventName);
            }
        }
    }

    public static void RemoveListener<T1, T2>(string eventName, Listener<T1, T2> listener)
    {
        if (GenericEvents<T1, T2>.events.ContainsKey(eventName))
        {
            GenericEvents<T1, T2>.events[eventName] -= listener;
            if (GenericEvents<T1, T2>.events[eventName] == null)
            {
                GenericEvents<T1, T2>.events.Remove(eventName);
            }
        }
    }

    public static void Invoke(string eventName, object sender)
    {
        if (GenericEvents.events.ContainsKey(eventName))
            GenericEvents.events[eventName].Invoke(sender);
        else
            Debug.Log($"No active listener for {eventName}");
    }

    public static void Invoke<T>(string eventName, object sender, T t)
    {
        if (GenericEvents<T>.events.ContainsKey(eventName))
            GenericEvents<T>.events[eventName].Invoke(sender, t);
        else
            Debug.Log($"No active listener for <{typeof(T)}> {eventName}");
    }

    public static void Invoke<T1, T2>(string eventName, object sender, T1 t1, T2 t2)
    {
        if (GenericEvents<T1, T2>.events.ContainsKey(eventName))
            GenericEvents<T1, T2>.events[eventName].Invoke(sender, t1, t2);
        else
            Debug.Log($"No active listener for <{typeof(T1)}, {typeof(T2)}> {eventName}");
    }

    public static void RemoveAllListeners(string eventName)
    {
        if (GenericEvents.events.ContainsKey(eventName))
            GenericEvents.events[eventName] = (t => { });
    }

    public static void RemoveAllListeners<T>(string eventName)
    {
        if (GenericEvents<T>.events.ContainsKey(eventName))
            GenericEvents<T>.events[eventName] = ((t1, t2) => { });
    }

    public static void RemoveAllListeners<T1, T2>(string eventName)
    {
        if (GenericEvents<T1, T2>.events.ContainsKey(eventName))
            GenericEvents<T1, T2>.events[eventName] = ((t1, t2, t3) => { });
    }

    private class GenericEvents
    {
        public static Dictionary<string, Listener> events = new Dictionary<string, Listener>();
    }

    private class GenericEvents<T>
    {
        public static Dictionary<string, Listener<T>> events = new Dictionary<string, Listener<T>>();
    }

    private class GenericEvents<T1, T2>
    {
        public static Dictionary<string, Listener<T1, T2>> events = new Dictionary<string, Listener<T1, T2>>();
    }
}