﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UserReviews : MonoBehaviour
{
    [SerializeField]
    private GameObject reviewsHolder;
    [SerializeField]
    private GameObject reviewPrefab;

    [SerializeField]
    private TextMeshProUGUI usernameHolder;
    [SerializeField]
    private TextMeshProUGUI reviewTextHolder;
    [SerializeField]
    private TMP_Dropdown ratingDropdown;

    [SerializeField]
    private TMP_InputField usernameInput;
    [SerializeField]
    private TMP_InputField reviewTextInput;

    [SerializeField]
    private GameObject errorImage;

    private Place lastPlace;

    private void OnEnable()
    {
        errorImage.SetActive(false);
    }

    public void LoadReviews(Place place)
    {
        foreach (Transform child in reviewsHolder.transform)
        {
            if (child.tag != "Persistent")
                Destroy(child.gameObject);
        }
        if (place.Comments != null)
        {
            foreach (var review in place.Comments)
            {
                GameObject reviewInstance = Instantiate(reviewPrefab, reviewsHolder.transform);
                string rating = "";
                for (int i = 0; i < review.rating; i++)
                {
                    rating += "*";
                }
                reviewInstance.GetComponent<TextMeshProUGUI>().text = $"{review.username} {rating}\n{review.body}";
            }
        }
        lastPlace = place;
    }

    public void B_Submit()
    {

        if(string.IsNullOrWhiteSpace(usernameHolder.text.Remove(usernameHolder.text.Length - 1)))
        {
            errorImage.SetActive(true);
            return;
        }
        errorImage.SetActive(false);
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            int rating = 0;
            switch (ratingDropdown.options[ratingDropdown.value].text)
            {
                case "*":
                    rating = 1;
                    break;
                case "**":
                    rating = 2;
                    break;
                case "***":
                    rating = 3;
                    break;
                case "****":
                    rating = 4;
                    break;
                case "*****":
                    rating = 5;
                    break;
            }

            Comments.AddComment(usernameHolder.text.Remove(usernameHolder.text.Length - 1),
            lastPlace.establishmentName, rating, 
            reviewTextHolder.text.Remove(reviewTextHolder.text.Length - 1));

            usernameInput.text = "";
            reviewTextInput.text = "";
            ratingDropdown.value = 0;

            LoadReviews(lastPlace);
        }
    }
}
