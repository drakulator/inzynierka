﻿using UnityEngine;

public class Comment : MonoBehaviour
{
    public CommentValues commentValues;
}

[System.Serializable]
public struct CommentValues
{
    public string username;
    public string placeName;
    public int rating;
    public string body;

    public CommentValues(string username, string placeName, int rating, string body)
    {
        this.username = username;
        this.placeName = placeName;
        this.rating = rating;
        this.body = body;
    }
}