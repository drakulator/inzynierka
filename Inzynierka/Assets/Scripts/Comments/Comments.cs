﻿using System.Collections.Generic;
using System.Data.SqlClient;
using UnityEngine;

public static class Comments
{
    public static List<CommentValues> GetComments()
    {
        var cb = new SqlConnectionStringBuilder();
        cb.DataSource = "comment.database.windows.net";
        cb.UserID = "drakulator";
        cb.Password = "Dupa1234";
        cb.InitialCatalog = "Comments";

        List<CommentValues> comments = new List<CommentValues>();
        using (var connection = new SqlConnection(cb.ConnectionString))
        {
            connection.Open();

            comments = SelectComments(connection);
        }

        return comments;
    }

    public static void AddComment(string username, string placeName, int rating, string body)
    {
        var cb = new SqlConnectionStringBuilder();
        cb.DataSource = "comment.database.windows.net";
        cb.UserID = "drakulator";
        cb.Password = "Dupa1234";
        cb.InitialCatalog = "Comments";

        using (var connection = new SqlConnection(cb.ConnectionString))
        {
            connection.Open();

            SubmitTsqlNonQuery(connection, "3 - Inserts",
               InsertComment(username, placeName, rating, body));
        }
        UserRatings.comments.Add(new CommentValues(username, placeName, rating, body));
    }

    static string InsertComment(string username, string placeName, int rating, string body)
    {
        return $@"
        INSERT INTO comments
           (Username, Place, Rating, Body)
              VALUES
           ('{username}', '{placeName}', {rating}, '{body}')
        ";
    }

    static string SelectCommentsSQL()
    {
        return @"
        SELECT
              CommentID,
              Username,
              Place,
              Rating,
              Body
           FROM
              comments
        ";
    }

    static List<CommentValues> SelectComments(SqlConnection connection)
    {
        string tsql = SelectCommentsSQL();

        List<CommentValues> comments = new List<CommentValues>();
        using (var command = new SqlCommand(tsql, connection))
        {
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    comments.Add(new CommentValues(
                       reader.GetString(1),
                       reader.GetString(2),
                       reader.GetInt32(3),
                       (reader.IsDBNull(3)) ? "" : reader.GetString(4))
                    );
                }
            }
        }
        return comments;
    }

    static void SubmitTsqlNonQuery(SqlConnection connection, string tsqlPurpose, string tsqlSourceCode, string parameterName = null, string parameterValue = null)
    {
        using (var command = new SqlCommand(tsqlSourceCode, connection))
        {
            if (parameterName != null)
            {
                command.Parameters.AddWithValue(  // Or, use SqlParameter class.
                    parameterName,
                    parameterValue);
            }
            int rowsAffected = command.ExecuteNonQuery();
            Debug.Log(rowsAffected + " = rows affected.");
        }
    }
}
