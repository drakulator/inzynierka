﻿using System.Collections.Generic;
using UnityEngine;

public class UserRatings : MonoBehaviour
{
    public static UserRatings Instance;
    public static List<CommentValues> comments;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        LoadComments();
    }

    private void LoadComments()
    {
        comments = new List<CommentValues>();
        if (Application.internetReachability == NetworkReachability.NotReachable)
            return;
        comments.AddRange(Comments.GetComments());
    }
}
